## 👋 Welcome!
This project contains all of the progress and deliverables that the Tufts University students made on their Capstone Project that was sponsored by GitLab. Please check out the project description file for more details.

## Findings
The results of this project include:
- Research & Design deliverables: https://gitlab.com/gitlab-org/ux-research/-/issues/2909
- Presentation: [Slides](https://docs.google.com/presentation/d/1bf3A-m6Keyr9RxypkY6ENmkEwwIrg4JBY1y8zM7hENY/edit#slide=id.g2d2a861d032_1_8) & [Recording](https://youtu.be/hP7KOFM7Wc4) 
- Figma designs: https://www.figma.com/proto/1Lmu73RpG0v9YGD1ePFWt5/GitLab-x-Tufts?page-id=3491%3A86945&type=design&node-id=3499-118680&viewport=276%2C811%2C0.08&t=e7M0rQgBRYeuaN3e-1&scaling=scale-down&starting-point-node-id=3499%3A118680&mode=design
