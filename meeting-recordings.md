**🔗 Important links:**

- [Kickoff slides from students](https://docs.google.com/presentation/d/1x89_RzRalOWtHpb8IOHMtl4JlPEB1A8l3kLeIQywVeY/edit?usp=sharing)
- [Google drive folder with deliverables](https://docs.google.com/presentation/d/1x89_RzRalOWtHpb8IOHMtl4JlPEB1A8l3kLeIQywVeY/edit?usp=sharing)
- [Agenda](https://docs.google.com/document/d/1taKOwlzzMiMHQ0Ism2TyVvEx8p9YqhEZLsMJjb5HDg0/edit?usp=sharing)

**YouTube recordings:**

[2024 Tufts Capstone playlist on GitLab Unfiltered](https://www.youtube.com/watch?v=NKXbAiKAi18&list=PL05JrBw4t0KrvlQvYVgjQX8nFNOWcXbd6)

Individual videos:

- Jan 31st: [GitLab Unfiltered Video](https://www.youtube.com/watch?v=NKXbAiKAi18)
- Feb 7th: [GitLab Unfiltered Video](https://youtu.be/zz0FcU-0r8w) | [Tufts Zoom Video](https://tufts.zoom.us/rec/share/m6Qly2BbiT6rrDGDwj2bI0wGvky5DQofC3OC-yuViSOHV7NtT0rgjFVLDynsXWwG.8pZe79JtHkjDoidD)
- Feb 14th: [GitLab Unfiltered video (private)](https://youtu.be/D8O87RCt9m4) | [Tufts Zoom Video](https://tufts.zoom.us/rec/share/ELwFC29a6b15yPtzK6ZG2Xee_HDAZyYX9jW1-MaZuWyz9VzP-r6W-sAyVucPq4df.CT6e3UdJ7rLzfAne)
- Feb 21st: [GitLab Unfiltered video](https://www.youtube.com/watch?v=0qJ1CrwIENY) | [Tufts Zoom Video](https://tufts.zoom.us/rec/share/7g-8xnGeopC7SLZ-_sQLdz4ibYfyZoj6XsJOFTnB2pcEjkGP3-g8U5KGdlwhysK5.wOkC4XGDkvQD0ANq)
- Feb 28th: 
- ...
- March 27th: [GitLab Unfiltered Video](https://www.youtube.com/watch?v=C1cCIyjlB60) | [Tufts Zoom Video](https://tufts.zoom.us/rec/share/dDtTq9MKsGSpuA7CFFgb0cV8iunmMS6Eiw4tdnc8as5kn0bgSzr8pIPFoAFr6tJ9.-kL-zlO3O7cnvnND)
- April 10th: [GitLab Unfiltered Video](https://www.youtube.com/watch?v=EDJpe1g3hs8) | [Tufts Zoom Video](https://tufts.zoom.us/rec/share/EZ-X1va0-Zrtyu9dGjNxpVwps64BoejcFZPRTnvehavBJWBHcuAjyZ8Kp0m1MPoP.k3CxNbcKcFlQWgmV?startTime=1712761558000)
- April 24th: [GitLab Unfiltered Video](https://www.youtube.com/watch?v=LZqhlD3P7n8) | [Tufts Zoom Video](https://tufts.zoom.us/rec/play/4c4j6SnbfjuQZpdoMQdEf3Vd7wR1WlGaZAWuhZo4c9_ynWXPQkU1YBqR4r-u-83J_pKXEPZ0r2UyKo7Z.hKQiwaERvBYa38SA?canPlayFromShare=true&from=share_recording_detail&continueMode=true&componentName=rec-play&originRequestUrl=https%3A%2F%2Ftufts.zoom.us%2Frec%2Fshare%2FMNqZ6AImmJvODcO-lSFe2sczPV24Hj49fQfhmEyku2KdfwgQkzEGl0V9s_W-Xw4.0VYoElfDXOqyXyvS)
- May 1st: [GitLab Unfiltered Video](https://www.youtube.com/watch?v=UjeeSHEcznw) | [Tufts Zoom Video](https://tufts.zoom.us/rec/share/4X0jnSyROiFnkvgjrXGiG4RT53tnyTu6x6kXSUlhvnC24ptCv_ODovmMyzDJUMPL.yt-bdSY92Cq334IX)