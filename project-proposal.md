## Explore how companies can improve their planning process by using GitLab Issue boards

### May 6th update

Based on discovery research and stakeholder feedback, the students decided to decrease the scope of the problem to focus on using boards to identify work that is in jeopardy of meeting planned delivery timing.

### Problem space

Planning is the first stage in the software development lifecycle and has a lot of influence of the quality of subsequent deliverables. The technology landscape is extremely competitive and inability to plan effectively to work on the right things can cost them a lot financially. This makes it very important for teams of all sizes to get the planning right.While the industry is flooded with project tracking tools and softwares, GitLab has an undisputed advantage over those being the One DevOps Platform. What this means for the teams using GitLab is—every part of the software development process, starting from planning and going all the way to shipping the software and setting up monitoring, happens at one place.
What are you expected to do?: You are required to evaluate the usefulness and impact of Issue board for agile sprint planning. You’ll speak with real GitLab users to understand their needs better around agile sprint planning and create proposals based on the insights gathered. The questions you’re expected to get answers for:

1. Users of issue boards?
    1. Yes
        1. How are users using the issue boards for sprint planning?
        2. What challenges do they face while using the feature?
        3. Waht are the organizationzal practices that they have to align their workflow with?
    2. No
        1. How are users doing agile sprint planning for their teams today?
        2. What are the challenges that the tool (GitLab) poses to their process today?
            1. How do they bypass those challenges in absence of an ideal solution?
        3. What are the organizational practices that they have to align to?

The proposals have to be designed using [Figma and Pajamas design system](https://design.gitlab.com/). You’ll then validate those proposals with GitLab users by running tests.
