## 📚 Learn about issue boards and the planning process

- [Issue boards documentation](https://docs.gitlab.com/ee/user/project/issue_board.html)
- [Direction page for the team who owns Issue boards](https://about.gitlab.com/direction/plan/portfolio_management/)
- [See one in action](https://gitlab.com/gitlab-org/gitlab/-/boards/4479209)

## 👋 Get to know your personas
- [Parker, Product Manager](https://handbook.gitlab.com/handbook/product/personas/#parker-product-manager)
- [Delaney, Development Team Lead/Engineering Manager](https://handbook.gitlab.com/handbook/product/personas/#delaney-development-team-lead)
- Secondary personas: [Sasha, Software Developer](https://handbook.gitlab.com/handbook/product/personas/#sasha-software-developer) & [Presley, Product Designer](https://handbook.gitlab.com/handbook/product/personas/#presley-product-designer)
- Job titles differ per company, therefore, focus on the tasks that these personas carry out rather than their job title.

## 📝 Use real data to help improve the Issue board experience

### Research insights - Usability Benchmarking

- [What is usability benchmarking?](https://handbook.gitlab.com/handbook/product/ux/ux-research/usability-benchmarking/)
- The workflows examined in this research method were:
  - Workflow 1: **Break down objectives** (this is less related to Boards)
  - Workflow 2: **Plan a sprint** which included these tasks: 
    - add items to upcoming sprint (user should add to an iteration) 
    - document levels of effort (user should add weight)
    - prioritize work (user sh0old add labels or use sorting capabilities)
    - add missing information (user should add labels)
  - Workflow 3: **Track status** which included these tasks: 
    - View current status of work (user should view epic or board details)
    - Determine teammate capacity (user should use board)
    - Update status of a work item (user should udpate label and close the item)
    - Determine a blocker's status (user should view the blocked linkage)
    - Find summary to share with leadership
    - Update priority levels for eng (user should add a scope label)

The following insights are the ones that had to do with the board experience:

#### Insight 1 (From workflow 2)

**Participants needed a way to more easily parse the different items of work on boards, and struggled when the title concatenation varied, making details such as weight not be spatially located consistently.** 
- Scanability is important for boards in particular because board workflows often include assessing a set of issues as a whole, determining where attention is needed, and other scan-and-address workflows. _Note: this was a lower priority need, as the task this affected scored high - participants generally did not struggle to add levels of effort (i.e., weight) to their issues._

**Supporting evidence:** A few participants missed seeing weight assigned to some issues in the board, since the location of the weight on the card was not consistent.
![quotes-and-screenshots](imgs/insight-1.png)

####  Insight 2 (From workflows 2 & 3)

**Participants really struggled to find, and return to, specific boards throughout workflows 2 and 3. They also struggled to use boards effectively, from missing information to slow load times.**
- Dropdown list of boards is difficult to read through and select from because it's small-view, and only organized in one way (ie alphabetized).
  - Within the dropdown, it's difficult to discover new boards or boards that might be relevant, and to remember where a board was.
- Different boards are in different places
  - Deep knowledge is required (of the way their GitLab instance is organized, their own organization's structure and naming conventions, if there are any) to find boards (mismatch in mental models).
  - Users cannot reliably browse and find their boards, instead, they need to remember where they were (recognition over recall).
- Participants did not seem to be able to search (ie global search) for a board based on title, keywords, or metadata.
- Board lists are limited, and hard to adjust or understand what boards can accomplish.
- Participants often did understand the board they were looking at at first glance, but were not sure how to adjust or edit for their use cases.
- Participants did not understand whether the scope filter would save or not.

**Supporting evidence:** 
| Add items to an upcoming sprint | Prioritize work | Determine teammate capacity |
| ---- | ---- | ---- |
| ![quotes-and-screenshots](imgs/insight-2-add-items.png) | ![quotes-and-screenshots](imgs/inight-2-prioritize-work.png) | ![quotes-and-screenshots](imgs/insight-2-determine.png) 

#### Insight 3 (From workflows 2 & 3)

**Participants often struggled to return to boards they already had found once. There was no one place to browse all boards, or easily search for boards based on the use case they were looking for, or to return to recently viewed boards.**

**Supporting evidence:** 
See insight 2.

#### Insight 4 (From workflow 2 mostly, but seen across all 3)

**Users often need to take the same action across multiple items at a time. While bulk editing is available and discoverable from the Issue List, it is not readily available from the Board or other views of multiple items scoped via filters.**
- Common use cases for bulk editing include updating all items within a sprint or iteration (eg adding to an iteration, changing due dates, adding labels), adding labels in general, assigning multiple items to one person.
- For example, improved bulk editing inline consistently across views...

**Supporting evidence:**
- This was evident across all 3 benchmark workflows, and especially prominent in Workflow 2: Planning and refining sprints.

| Plan and refine a sprint | Add items to upcoming sprint | Prioritize work |
| ------ | ------ | ------ |
|  ![quotes-and-screenshots](imgs/insight-4-plan-and-refine.png)      | ![quotes-and-screenshots](imgs/insight-2-add-items.png)      |   ![quotes-and-screenshots](imgs/inight-2-prioritize-work.png)  |

### Research insights - Audit from Prior Studies

#### How do users utilize issue boards?

For capacity planning, sprint planning, Scrum, and Kanban.

<details><summary>Quotes from users</summary>

> Finds it difficult to assign issue weights. Doesn't use the milestone dashboard generally. almost never uses lists. Usually works through custom boards.Tries to have a priority for issue lists. GitLab doesn't really have a priority concept for issues. Has boards: 0 Sprint status, 1 Sprint Planning, 2 Themes,  3 Priorities, 4 Milestones.  Currently isn't able to add issues from the board (just updated to 12.2). High-level planning done through boards. Uses scoped labels for sprints and views them in the board. Has the current sprint, next sprint, and "blocked" work in progress columns on the boards.  Once the Sprint is in progress, he almost exclusively works from the "Sprint status" board. He uses scoped labels to show "Progress: Undefined." Undefinied is the startpoint. Once he adds tasks to the issue, the Progress status becomes "Defined," they also have a "Started" label.

> Issue Boards are used for stack-ranking. They’d like to do this at the Epic level too.

> 5 Dev teams are using GitLab for their planning  They were very Waterfall a few years ago but now are undefined  They have 4 teams names after non-standard colors  2 boards for each team Separate board for big-bucket milestone planning  They have different GitLab Groups for organizing the teams into those groups  Fixed-capacity model. They don’t change capacity generally.  They have not really adopted weights. Their teams have a good sense of their velocity and it wastes less time to not spend energy calculating. They think it’d take extra time to figure out estimation.

> They organize work in the Backlog column and then it moves into different columns according to priority. Typical flow goes from Issues to Merge Requests. They have a Kanban workflow and are using scoped labels for columns. They also have other scoped labels. 

> They have a Product Owner view that sits at the top level. They can see all the work by units. Their ideal scenario is that they would have all their boards at the highest level so they can consolidate all those views into one space

> Scrum method. Everyone decides what will be done next and they use their issue board to capture this information. There’s no complex planning process. Typically all the developers and their two bosses are involved in the planning process. Because they are a small company, they can do this in this way. They have a look at all the issues on the board and decide what is important from there.

</details>

#### The lack of real-time updates ("auto-refresh") in Issue Boards causes errors during team planning sessions 

👉 GitLab issue: https://gitlab.com/gitlab-org/gitlab/-/issues/16020

<details><summary>Quotes from users</summary>

> I would honestly have expected much more people complaining about the lack of realtime updates. This is a constant source of frustration for us as a remote team. During backlog grooming and planning meetings, there are often multiple people who are moving issues and renaming them at the same time. So I understand that refreshing the whole page is costly, but we need to do that anyways at the moment.

> Is there a way to get the cards updated in real time eg, If I am doing changes and moving cards, can that update for the developers as well? If I move a card, other users need to refresh their page to see the update, Trello, for example, will update automatically so one user changes, it updates for everyone without needing to refresh page.

> Issue Boards are insufficient facilitating collaborative planning. Constant page reloads are not intuitive, especially if there are multiple people individually looking at the same board during a meeting.

> It just doesn't make sense for boards not to be updated in realtime; this is not a "nice to have", it has already led to many conflicts in our development and testing activities.

> This is really needed for me and my team. I alway have a tab open in the browser for the board and this would help me alot. It's especially annoying when you forget you refresh and make a "bad" change due to not seeing the latest data.

</details>

#### Users want to see more automation in Issue Boards to reduce burden of manually managing Issue metadata

👉 GitLab issue: https://gitlab.com/gitlab-org/gitlab/-/issues/241773

<details><summary>Quotes from users</summary>

> He'd like to see GitLab's issue boards behaving more like JIRA, particularly in terms of automation and creating templates. This would be especially useful for students. He needs to occasionally set triggers to remind students to complete projects. For example, some of his students are developing something based on GPO processing and because of that they have to submit logs. If he wants to log something that went from the student's computer to his computer and then to the company itself, it's currently manual in GitLab whereas it's automated in JIRA

> Doesn't experience limitations for his current purposes. However, if he was trying to use Boards to manage a team of 20-30 people, he'd encounter more problems (due to the lack of automation). He does not want to shuffle and relabel everything by hand. He can manage this now when the team is small but not if he had that bigger team.

> You need to add board automation. Stuff like "If an issue is labeled 'x' and then labeled 'y' do 'z'." You can take a look at Azure DevOps to see a great setup. Some examples from our workflow: - If an issue is label changes from 'c::READY TO BEGIN' to 'c::IN PROGRESS', assign the current user - Prevent an issue from moving from 'c::IN PROGRESS' to 'c::READY FOR CODE REVIEW' if the issue still has incomplete tasks  

> Automations like those available in Azure DevOps. Things like assigning a user when a label is added, blocking moving columns if an issue still has incomplete tasks, etc.

> Ability to create rules based on workflow. For example, “If a card moves from “ready to begin” to “in progress” and nobody is assigned, the current user (the person dragging the card in) should be assigned.” Just some simple automation at these touchpoints.

</details>

#### Users want to have "workflow" labels automatically removed from issues in the Issue Board, when they are closed via MRs

👉 GitLab issue: https://gitlab.com/gitlab-org/gitlab/-/issues/241773 

Users feel frustrated when issues with labels such as “Todo,” “Doing,” “Done,” etc are still present on issues that have been automatically closed through an MR. This causes confusion and they have to manually remove the labels.

<details><summary>Quotes from users</summary>

> I am left resorting to going to my closed issues and removing the Done label manually and consequently this is breaking my workflow. Until this is fixed, it really makes the Issue Board useless for me and I am just going to stick with the Milestone view for issues.

> what are the issue boards for? Are they a way to filter existing issues by label or are they supposed to represent a workflow using physical boards where one would move a task to different boards as the task progresses? If they're just a way to filter by labels, then I don't see them being very useful to us as they're just adding confusion and not doing what we want. If they're supposed to represent workflow, then I honestly don't see why they are based on labels. Using labels means that a single issue can be in multiple workflow states at the same time because it can have multiple labels applied. Clearly a task should not be in "ToDo" and "Working" at the same time or "Working" and "Done", but this is possible because they are based on labels. And there now seems to be additional effort required to determine what previous labels to remove to ensure correctness. For boards to be useful to us, I think an issue should be assigned to a single board at a time (keeping labels independent of workflow) and a per project setting used to specify which board to move tasks to that are closed by MR or comment based fixes/closes. Technically the workflow should be based on the type of task, but having the above ability would at least make the boards useful to the majority of users it seems.

> If someone looks at the issue without going through the board view it may be closed, but it may be tagged as in review. Which could mean, that it is done and has been reviewed, or that someone accidentally closed the issue and it should be reopened. It caused a lot of confusion in our last project, which ultimately left us to abandon the GitLab board in favor of a third party board.

> I think considering todo and doing are the default labels, the default behaviour should be removing all labels on closing an issue.

> Please allow us to define on labels if they are to be removed or should stay on issue closure, we have a lot of overview boards where we use labels to place issues into a category (frontend/backend/server etc) these labels are used even after an issue is closed to figure out which changes are relevant to end users of the system and which ones they don't need to know about because it was just a simple chore/cleanup.

</details>

#### Users want an Issue Board view that gives them better visibility into the status of Issues across all projects in their instance 

👉 GitLab issue: https://gitlab.com/gitlab-org/gitlab/-/issues/242018

<details><summary>Quotes from users</summary>

> It would be really nice to be able to have an overview of issues/boards across ALL projects. We have project managers who assign work on a daily/weekly basis to development team and need to see which developers are free. Ideally there would be a permission level that could be added to a user - "Project Manager"/"Issue Manager" this would then give an ability to see and change issues across ALL projects. 

> As many of us, as developer I try to organize my work & time. But in our current gitlab organization I'm working on many gitlab groups projects. So today to see all my issues I could use https://<gitlab-instance>/dashboard/issues?assignee_id=<my-user-id> but is simply a flat list. I would like to be able to get the same thing but wrapped inside issue board (maybe https://<gitlab-instance>/dashboard/board?assignee_id=<my-user-id>) to be able to handle my work globally instead of browsing each group and/or project and sum up result inside my mind. User centric instead of group/project centric.

> Waiting for this eagerly before shifting in to Gitlab. Having one team handling multiple repos / projects, it is easier to manage the team's tasks from a macro perspective i.e. a board which spans all projects.

> It would be really nice to be able to have an overview of issues/boards across ALL projects. We have project managers who assign work on a daily/weekly basis to development team and need to see which developers are free. Ideally there would be a permission level that could be added to a user - "Project Manager"/"Issue Manager" this would then give an ability to see and change issues across ALL projects. 

> GitLab boards are not very friendly for getting an overview of developer progress and no data on the actual time they have spent on each issue. There are no timestamps for issues so he uses JIRA for this tracking. Issue boards are currently more of a macro-level planning tool. He currently uses issue boards because he doesn't have an alternative. He would go for JIRA for everything related to planning and tracking developer's time and hook it up to GitLab's source code management features.

</details>

#### Users want to see more data about the time an Issue spends in a particular list on the Issue Board

👉 GitLab issue: https://gitlab.com/gitlab-org/gitlab/-/issues/242038 

<details><summary>Quotes from users</summary>

> GitLab boards are not very friendly for getting an overview of developer progress and no data on the actual time they have spent on each issue. There are no timestamps for issues so he uses JIRA for this tracking. Issue boards are currently more of a macro-level planning tool. He currently uses issue boards because he doesn't have an alternative. He would go for JIRA for everything related to planning and tracking developer's time and hook it up to GitLab's source code management features.

> Metrics they are generating: He’s trying to say “What is active time (something not in a waiting state) across columns?” They’d like to calculate work in progress, cycle time, and throughput. They want timestamps when something moves to a different column. 

> They’d like to see “aging” - the board starts changing color based on amount of time issues have spent on the board. This would help them quickly look at the board and see the status of things. They list Azure DevOps as an example of aging.

> see more integration with GitLab CI in the board view. He'd like to see the status of the pipeline on the card, with the run number underneath.He'd like to see it in the board so that if someone is working on it and actively developing it, team members can see how they're progressing and intervene if they need to. Visits the board view 1-3 times a day.

> They’d like to be able to represent the status of an Issue within the column. The column label may still be appropriate but they want to show the status simultaneously. 

</details>

#### Feature requests for Issue Boards 

Key takeaways: 
- Swim lanes in Boards
- Real-time updates
- Board automation
- Epics in Boards
- Automatically remove workflow/status labels from closed issues

### Anecdotal feedback from Boards Users

#### Features in other kanban board products are missing

Users accustomed to other kanban board products like Jira are surprised to find that GitLab's boards are missing several features common to others:

- Boards must be within a single project or group (cannot span multiple projects across groups)
- Boards cannot be based on complex filters
- If the "Closed" column is hidden from the board, then there is no way to close issues that appear on the board without opening them (see below), but when shown, the "Closed" column shows all closed issues matching the board query, which can be a very long list. In other products, a similar closed/done list can be customized to only show issues closed within the last X days, allowing a drag and drop target for closing an issue in the UI without creating a huge column of data that must be loaded unnecessarily.
- There is no right-click contextual menu for issues in a board for common actions (such as closing the issue, assigning it, editing labels, sharing a link to it...)
- There is no way to open an issue on a board in a modal. The issue must either be opened in a minimal sidebar of meta information (that, notably, excludes the issue description and activity) or the user must open the issue directly
  - By default, issues open in the same tab as the board if you click their title, causing context loss
- No bulk actions (as mentioned above in Insight 4)
- Lists (that is, columns) cannot be based on custom queries (for example, issues that have blocking relationships to other issues)
- When lists are based on labels that represent workflow steps, moving an issue to the "Closed" column does not remove the workflow label that was previously applied to it, meaning it must be manually removed.
- Tasks do not show on boards
- There are no swimlanes in boards (for example, by user, or by label, or by weight)
- When working in an Agile scrum team, Boards in GitLab are only weakly associated with a sprint/iteration, and so lack data on time remaining, burndown etc (that information is available elsewhere in GitLab).
- There is no concept of a private board. All boards are visible to all users who can access the project or group that the board is in (this also causes clutter and navigation difficulty)
